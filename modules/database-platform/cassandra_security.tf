resource "aws_security_group" "cassandra_seed_sg" {
  name_prefix = "${module.common.globals["Project"]}-${module.common.globals["Environment"]}-cassandra-seed"
  vpc_id      = module.networking.common["VpcId"]
  description = "${module.common.globals["Project"]}-${module.common.globals["Environment"]}-cassandra-seed-sg"
}

### INCOMING

resource "aws_security_group_rule" "cassandra_seed_sg_ingress_9042_tcp_mgmt" {
  description              = "Allow access from mgmt on 9042 TCP"
  type                     = "ingress"
  from_port                = "9042"
  to_port                  = "9042"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_mgmt_sg.id
  security_group_id        = aws_security_group.cassandra_seed_sg.id
}

resource "aws_security_group_rule" "cassandra_seed_sg_ingress_8080_tcp_mgmt" {
  description              = "Allow Reaper UI access from mgmt on 8080 TCP"
  type                     = "ingress"
  from_port                = "8080"
  to_port                  = "8080"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_mgmt_sg.id
  security_group_id        = aws_security_group.cassandra_seed_sg.id
}

resource "aws_security_group_rule" "cassandra_seed_sg_ingress_22_tcp" {
  description       = "Allow cluster intercommunication on port 22 TCP"
  type              = "ingress"
  from_port         = "22"
  to_port           = "22"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.cassandra_seed_sg.id
}

resource "aws_security_group_rule" "cassandra_seed_sg_ingress_7000_tcp" {
  description       = "Allow cluster intercommunication on port 7000 TCP"
  type              = "ingress"
  from_port         = "7000"
  to_port           = "7000"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.cassandra_seed_sg.id
}

resource "aws_security_group_rule" "cassandra_seed_sg_ingress_7001_tcp" {
  description       = "Allow cluster intercommunication on port 7001 TCP"
  type              = "ingress"
  from_port         = "7001"
  to_port           = "7001"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.cassandra_seed_sg.id
}

resource "aws_security_group_rule" "cassandra_seed_sg_ingress_7199_tcp" {
  description       = "Allow cluster intercommunication on port 7199 TCP"
  type              = "ingress"
  from_port         = "7199"
  to_port           = "7199"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.cassandra_seed_sg.id
}

resource "aws_security_group_rule" "cassandra_seed_sg_ingress_9042_tcp" {
  description       = "Allow cluster intercommunication on port 9042 TCP"
  type              = "ingress"
  from_port         = "9042"
  to_port           = "9042"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.cassandra_seed_sg.id
}

resource "aws_security_group_rule" "cassandra_seed_sg_application_ingress" {
  description       = "Allow application CIDRs to communicate with cluster on TCP 9042"
  type              = "ingress"
  from_port         = "9042"
  to_port           = "9042"
  protocol          = "tcp"
  cidr_blocks       = var.ALLOWED_CIDR_BLOCKS
  security_group_id = aws_security_group.cassandra_seed_sg.id
}

resource "aws_security_group_rule" "cassandra_seed_sg_ingress_9100_tcp_mgmt" {
  description              = "Allow cluster TCP access 9100 from mgmt node"
  type                     = "ingress"
  from_port                = "9100"
  to_port                  = "9100"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_mgmt_sg.id
  security_group_id        = aws_security_group.cassandra_seed_sg.id
}

resource "aws_security_group_rule" "cassandra_seed_sg_ingress_7070_tcp_mgmt" {
  description              = "Allow cluster TCP access 7070 from mgmt node"
  type                     = "ingress"
  from_port                = "7070"
  to_port                  = "7070"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_mgmt_sg.id
  security_group_id        = aws_security_group.cassandra_seed_sg.id
}

### OUTGOING

resource "aws_security_group_rule" "cassandra_seed_sg_egress_22_tcp" {
  description       = "Allow cluster intercommunication on port 22 TCP"
  type              = "egress"
  from_port         = "22"
  to_port           = "22"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.cassandra_seed_sg.id
}

resource "aws_security_group_rule" "cassandra_seed_sg_egress_7000_tcp" {
  description       = "Allow cluster intercommunication on port 7000 TCP"
  type              = "egress"
  from_port         = "7000"
  to_port           = "7000"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.cassandra_seed_sg.id
}

resource "aws_security_group_rule" "cassandra_seed_sg_egress_7001_tcp" {
  description       = "Allow cluster intercommunication on port 7001 TCP"
  type              = "egress"
  from_port         = "7001"
  to_port           = "7001"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.cassandra_seed_sg.id
}

resource "aws_security_group_rule" "cassandra_seed_sg_egress_7199_tcp" {
  description       = "Allow cluster intercommunication on port 7199 TCP"
  type              = "egress"
  from_port         = "7199"
  to_port           = "7199"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.cassandra_seed_sg.id
}

resource "aws_security_group_rule" "cassandra_seed_sg_egress_9042_tcp" {
  description       = "Allow cluster intercommunication on port 9042 TCP"
  type              = "egress"
  from_port         = "9042"
  to_port           = "9042"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.cassandra_seed_sg.id
}
