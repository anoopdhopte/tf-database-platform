locals {
  aurora_pg_enforce_client_ssl = var.AURORA_PG_ENFORCE_CLIENT_SSL == true ? [{ apply_method = "immediate", name = "rds.force_ssl", value = "1" }] : []
}

module "db" {
  source              = "git::https://bitbucket.org/anoopdhopte/rds.git//modules/aurora?ref=2.0.0"
  globals                  = module.common.globals
  networking               = module.networking.common
  DB_NAME                  = var.AURORA_DB_NAME
  ADMIN_USER               = var.AURORA_ADMIN_USER
  ADMIN_PASSWORD           = var.AURORA_DATABASE_ADMIN_PASSWORD
  SUBNETS                  = module.networking.data_subnet_ids
  ALLOWED_CIDR_BLOCKS      = concat([var.VPC_CIDR_BLOCK], var.ALLOWED_CIDR_BLOCKS)
  AUTOSCALING_ENABLED      = var.AURORA_AUTOSCALING_ENABLED
  AUTOSCALING_MIN_CAPACITY = var.AURORA_AUTOSCALING_MIN_CAPACITY
  AUTOSCALING_MAX_CAPACITY = var.AURORA_AUTOSCALING_MAX_CAPACITY
  PUBLICLY_ACCESSIBLE      = false
  ENGINE                   = var.AURORA_ENGINE
  ENGINE_VERSION           = var.AURORA_ENGINE_VERSION
  ENGINE_FAMILY            = var.AURORA_ENGINE_FAMILY
  ENGINE_MODE              = var.AURORA_ENGINE_MODE
  INSTANCE_TYPE            = var.AURORA_INSTANCE_TYPE
  STORAGE_ENCRYPTED        = true
  KMS_KEY_ARN              = module.encryption.kms_key_id
  RDS_MONITORING_INTERVAL  = var.AURORA_ENHANCED_MONITORING_INTERVAL
  RDS_MONITORING_ROLE_ARN  = aws_iam_role.rds_enhanced_monitoring_role.arn
}

# tflint-ignore: terraform_naming_convention
module "db-pg" {
  source              = "git::https://bitbucket.org/anoopdhopte/rds.git//modules/aurora?ref=2.0.0"
  globals                  = module.common.globals
  networking               = module.networking.common
  DB_NAME                  = var.AURORA_PG_DB_NAME
  ADMIN_USER               = var.AURORA_PG_ADMIN_USER
  ADMIN_PASSWORD           = var.AURORA_PG_DATABASE_ADMIN_PASSWORD
  SUBNETS                  = module.networking.data_subnet_ids
  ALLOWED_CIDR_BLOCKS      = concat([var.VPC_CIDR_BLOCK], var.ALLOWED_CIDR_BLOCKS_WR)
  AUTOSCALING_ENABLED      = var.AURORA_PG_AUTOSCALING_ENABLED
  AUTOSCALING_MIN_CAPACITY = var.AURORA_PG_AUTOSCALING_MIN_CAPACITY
  AUTOSCALING_MAX_CAPACITY = var.AURORA_PG_AUTOSCALING_MAX_CAPACITY
  PUBLICLY_ACCESSIBLE      = false
  ENGINE                   = var.AURORA_PG_ENGINE
  ENGINE_VERSION           = var.AURORA_PG_ENGINE_VERSION
  ENGINE_FAMILY            = var.AURORA_PG_ENGINE_FAMILY
  ENGINE_MODE              = var.AURORA_PG_ENGINE_MODE
  INSTANCE_TYPE            = var.AURORA_PG_INSTANCE_TYPE
  STORAGE_ENCRYPTED        = true
  KMS_KEY_ARN              = module.encryption.kms_key_id
  RDS_MONITORING_INTERVAL  = var.AURORA_ENHANCED_MONITORING_INTERVAL
  RDS_MONITORING_ROLE_ARN  = aws_iam_role.rds_enhanced_monitoring_role.arn
  CLUSTER_PARAMETERS       = concat(local.aurora_pg_enforce_client_ssl)
}

resource "aws_iam_role" "rds_enhanced_monitoring_role" {
  name               = "enhanced_${var.ENVIRONMENT}_rds_monitoring_role"
  assume_role_policy = data.aws_iam_policy_document.rds_enhanced_monitoring.json
}

resource "aws_iam_role_policy_attachment" "rds_enhanced_monitoring_role" {
  role       = aws_iam_role.rds_enhanced_monitoring_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}

data "aws_iam_policy_document" "rds_enhanced_monitoring" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["monitoring.rds.amazonaws.com"]
    }
  }
}
