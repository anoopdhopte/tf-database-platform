module "logs_bucket" {
  source                  = "git::https://bitbucket.org/anoopdhopte/s3.git//modules/logs?ref=1.1.1"
  globals         = module.common.globals
  BUCKET_NAME     = "-${var.AWS_REGION}"
  EXPIRATION_DAYS = "30"
  SSE_ALGORITHM   = "AES256"
  MFA_DELETE      = var.MFA_ENABLED
}
