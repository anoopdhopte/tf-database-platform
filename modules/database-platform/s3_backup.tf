locals {
  # tflint-ignore: terraform_naming_convention
  BUCKET_NAME = "${module.common.globals["Project"]}-${module.common.globals["Environment"]}-${module.common.globals["AccountName"]}-${module.common.globals["Region"]}-cassandra-backups"
}

data "template_file" "cassandra_s3_backup_bucket_policy" {
  template = file("${path.module}/files/backup-bucket-policy")
  vars = {
    bucket_name = local.BUCKET_NAME
  }
}

module "cassandra_s3_backup_bucket" {
  source          = "git::https://bitbucket.org/anoopdhopte/s3.git//modules/bucket?ref=1.0.2"
  globals         = module.common.globals
  EXPIRATION_DAYS = var.CASSANDRA_BACKUP_RETENTION
  BUCKET_NAME     = local.BUCKET_NAME
  SSE_ALGORITHM   = "AES256"
  BUCKET_POLICY   = data.template_file.cassandra_s3_backup_bucket_policy.rendered
}

