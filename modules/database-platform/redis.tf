module "redis" {
  source                 = "git::https://bitbucket.org/anoopdhopte/elasticache.git//modules/redis?ref=1.0.6"
  globals                = module.common.globals
  networking             = module.networking.common
  KMS_KEY_ID             = module.encryption.kms_key_id
  SUBNET_IDS             = module.networking.data_subnet_ids
  INGRESS_CIDR_BLOCKS    = concat([var.VPC_CIDR_BLOCK], var.ALLOWED_CIDR_BLOCKS)
  REPLICATION_GROUP_ID   = var.REDIS_GROUP_ID
  AUTH_TOKEN             = var.REDIS_AUTH_TOKEN
  NODE_GROUPS            = var.REDIS_NODE_GROUPS
  NODE_TYPE              = var.REDIS_NODE_TYPE
  ENGINE_VERSION         = var.REDIS_ENGINE_VERSION
  CUSTOM_PARAMETER_GROUP = var.REDIS_CUSTOM_PARAMETER_GROUP
  APPLY_IMMEDIATELY      = var.REDIS_APPLY_IMMEDIATELY
}
