resource "aws_security_group" "opendj_sg" {
  name_prefix = "${module.common.globals["Project"]}-${module.common.globals["Environment"]}-opendj"
  vpc_id      = module.networking.common["VpcId"]
  description = "${module.common.globals["Project"]}-${module.common.globals["Environment"]}-opendj-sg"
}

### INCOMING

## mgmt
resource "aws_security_group_rule" "opendj_sg_ingress_22_tcp_mgmt" {
  description              = "Allow cluster SSH access from mgmt on 22 TCP"
  type                     = "ingress"
  from_port                = "22"
  to_port                  = "22"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_mgmt_sg.id
  security_group_id        = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_sg_ingress_1389_tcp_mgmt" {
  description              = "Allow access from mgmt on 1389 TCP"
  type                     = "ingress"
  from_port                = "1389"
  to_port                  = "1389"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_mgmt_sg.id
  security_group_id        = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_sg_ingress_1636_tcp_mgmt" {
  description              = "Allow access from mgmt on 1636 TCP"
  type                     = "ingress"
  from_port                = "1636"
  to_port                  = "1636"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_mgmt_sg.id
  security_group_id        = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_sg_ingress_4444_tcp_mgmt" {
  description              = "Allow access from mgmt on 4444 TCP"
  type                     = "ingress"
  from_port                = "4444"
  to_port                  = "4444"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_mgmt_sg.id
  security_group_id        = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_sg_ingress_7946_tcp_mgmt" {
  description              = "Allow access from mgmt on 7946 TCP"
  type                     = "ingress"
  from_port                = "7946"
  to_port                  = "7946"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_mgmt_sg.id
  security_group_id        = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_sg_ingress_8989_tcp_mgmt" {
  description              = "Allow access from mgmt on 8989 TCP"
  type                     = "ingress"
  from_port                = "8989"
  to_port                  = "8989"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_mgmt_sg.id
  security_group_id        = aws_security_group.opendj_sg.id
}

## self
resource "aws_security_group_rule" "opendj_sg_ingress_1389_tcp" {
  description       = "Allow cluster intercommunication on 1389 TCP"
  type              = "ingress"
  from_port         = "1389"
  to_port           = "1389"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_sg_ingress_1636_tcp" {
  description       = "Allow cluster intercommunication on 1636 TCP"
  type              = "ingress"
  from_port         = "1636"
  to_port           = "1636"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_sg_ingress_4444_tcp" {
  description       = "Allow cluster intercommunication on 4444 TCP"
  type              = "ingress"
  from_port         = "4444"
  to_port           = "4444"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_sg_ingress_7946_tcp" {
  description       = "Allow cluster intercommunication on 7946 TCP"
  type              = "ingress"
  from_port         = "7946"
  to_port           = "7946"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_sg_ingress_8989_tcp" {
  description       = "Allow cluster intercommunication on port 8989 TCP"
  type              = "ingress"
  from_port         = "8989"
  to_port           = "8989"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.opendj_sg.id
}

## application
resource "aws_security_group_rule" "opendj_application_ingress_1389" {
  description       = "Allow application CIDRs to communicate with cluster on TCP 1389"
  type              = "ingress"
  from_port         = "1389"
  to_port           = "1389"
  protocol          = "tcp"
  cidr_blocks       = var.ALLOWED_CIDR_BLOCKS_OPENDJ
  security_group_id = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_application_ingress_11636" {
  description       = "Allow application CIDRs to communicate with cluster on TCP 1636"
  type              = "ingress"
  from_port         = "1636"
  to_port           = "1636"
  protocol          = "tcp"
  cidr_blocks       = var.ALLOWED_CIDR_BLOCKS_OPENDJ
  security_group_id = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_application_ingress_4444" {
  description       = "Allow application CIDRs to communicate with cluster on TCP 4444"
  type              = "ingress"
  from_port         = "4444"
  to_port           = "4444"
  protocol          = "tcp"
  cidr_blocks       = var.ALLOWED_CIDR_BLOCKS_OPENDJ
  security_group_id = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_application_ingress_7946" {
  description       = "Allow application CIDRs to communicate with cluster on TCP 7946"
  type              = "ingress"
  from_port         = "7946"
  to_port           = "7946"
  protocol          = "tcp"
  cidr_blocks       = var.ALLOWED_CIDR_BLOCKS_OPENDJ
  security_group_id = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_application_ingress_8989" {
  description       = "Allow application CIDRs to communicate with cluster on TCP 8989"
  type              = "ingress"
  from_port         = "8989"
  to_port           = "8989"
  protocol          = "tcp"
  cidr_blocks       = var.ALLOWED_CIDR_BLOCKS_OPENDJ
  security_group_id = aws_security_group.opendj_sg.id
}

### OUTGOING

resource "aws_security_group_rule" "opendj_sg_egress_1389_tcp" {
  description       = "Allow cluster intercommunication on port 1389 TCP"
  type              = "egress"
  from_port         = "1389"
  to_port           = "1389"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_sg_egress_1636_tcp" {
  description       = "Allow cluster intercommunication on port 1636 TCP"
  type              = "egress"
  from_port         = "1636"
  to_port           = "1636"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_sg_egress_4444_tcp" {
  description       = "Allow cluster intercommunication on port 4444 TCP"
  type              = "egress"
  from_port         = "4444"
  to_port           = "4444"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_sg_egress_7946_tcp" {
  description       = "Allow cluster intercommunication on port 7946 TCP"
  type              = "egress"
  from_port         = "7946"
  to_port           = "7946"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.opendj_sg.id
}

resource "aws_security_group_rule" "opendj_sg_egress_8989_tcp" {
  description       = "Allow cluster intercommunication on port 8989 TCP"
  type              = "egress"
  from_port         = "8989"
  to_port           = "8989"
  protocol          = "tcp"
  self              = true
  security_group_id = aws_security_group.opendj_sg.id
}

