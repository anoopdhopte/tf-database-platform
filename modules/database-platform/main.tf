resource "random_string" "id" {
  length  = 4
  special = false
  upper   = true
  lower   = false
  number  = false
}

module "common" {
  source          = "git::https://bitbucket.org/anoopdhopte/common.git//modules?ref=1.0.1"
  ACCOUNT_NAME    = var.ACCOUNT_NAME
  AWS_REGION      = var.AWS_REGION
  ENVIRONMENT     = var.ENVIRONMENT
  PROJECT         = var.PROJECT
  PUBLIC_KEY_PATH = var.PUBLIC_KEY_PATH != "" ? var.PUBLIC_KEY_PATH : "${path.module}/files/dba-key.pub"
}

module "encryption" {
  source              = "git::https://bitbucket.org/anoopdhopte/encryption.git//modules?ref=1.0.3"
  globals             = module.common.globals
  KMS_KEY_NAME_SUFFIX = random_string.id.result
}

module "networking" {
  source                  = "git::https://bitbucket.org/anoopdhopte/vpc.git//modules/networking?ref=2.0.2"
  globals                 = module.common.globals
  VPC_CIDR_BLOCK          = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.APPLICATION_CIDR_BLOCKS
  DATA_CIDR_BLOCKS        = var.DATA_CIDR_BLOCKS
  FLOW_LOGS_ENABLED       = var.FLOW_LOGS_ENABLED
  FLOW_LOGS_BUCKET_ARN    = module.logs_bucket.s3_bucket_arn
  FLOW_LOGS_TRAFFIC_TYPE  = var.FLOW_LOGS_TRAFFIC_TYPE
  USE_INTERNET_GATEWAY    = true
  USE_NAT_GATEWAY         = true
  SINGLE_NAT_GATEWAY      = true
}

module "dns_zone" {
  source                  = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/zone_public?ref=1.0.2"
  globals                 = module.common.globals
  DNS_ZONE_NAME           = var.DNS_ZONE_NAME
  CREATE_ZONE_CERTIFICATE = false
}
