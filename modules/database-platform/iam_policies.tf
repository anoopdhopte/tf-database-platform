locals {
  vms_instance_ids = [
    module.cassandra_mgmt_vm.instance_id,
    module.cassandra_seed1_vm.instance_id,
    module.cassandra_seed2_vm.instance_id,
    module.cassandra_seed3_vm.instance_id,
    module.cassandra_wr_seed1_vm.instance_id,
    module.cassandra_wr_seed2_vm.instance_id,
    module.cassandra_wr_seed3_vm.instance_id,
    module.opendj_node1_vm.instance_id,
    module.opendj_node2_vm.instance_id,
  ]
  vms_instance_role_names = [
    module.cassandra_mgmt_vm.instance_role_name,
    module.cassandra_seed1_vm.instance_role_name,
    module.cassandra_seed2_vm.instance_role_name,
    module.cassandra_seed3_vm.instance_role_name,
    module.cassandra_wr_seed1_vm.instance_role_name,
    module.cassandra_wr_seed2_vm.instance_role_name,
    module.cassandra_wr_seed3_vm.instance_role_name,
    module.opendj_node1_vm.instance_role_name,
    module.opendj_node2_vm.instance_role_name,
  ]
}

data "aws_caller_identity" "current" {}

######### Medusa backup

data "template_file" "cassandra_s3_bucket_medusa_policy" {
  template = file("${path.module}/files/medusa-iam-policy")
  vars = {
    bucket = local.BUCKET_NAME
  }
}

resource "aws_iam_policy" "medusa_backup_policy" {
  name   = "medusa-backup-policy-${module.common.globals["Project"]}-${module.common.globals["Environment"]}-${module.common.globals["Region"]}"
  policy = data.template_file.cassandra_s3_bucket_medusa_policy.rendered
}

resource "aws_iam_role_policy_attachment" "medusa_backup_policy_attach" {
  count      = length(local.vms_instance_role_names)
  policy_arn = aws_iam_policy.medusa_backup_policy.arn
  role       = element(local.vms_instance_role_names, count.index)
}

######### SSM
data "template_file" "ssm_access_policy" {
  template = file("${path.module}/files/ssm-access-iam-policy")
}

data "template_file" "ssm_start_session_policy" {
  count    = length(local.vms_instance_ids)
  template = file("${path.module}/files/ssm-start-session-iam-policy")
  vars = {
    account  = data.aws_caller_identity.current.account_id
    region   = var.AWS_REGION
    instance = element(local.vms_instance_ids, count.index)
  }
}

resource "aws_iam_policy" "vm_ssm_access_policy" {
  name   = "SSM-access-policy-${module.common.globals["Project"]}-${module.common.globals["Environment"]}-${module.common.globals["Region"]}"
  policy = data.template_file.ssm_access_policy.rendered
}

resource "aws_iam_role_policy_attachment" "vm_ssm_access_policy_attach" {
  count      = length(local.vms_instance_role_names)
  policy_arn = aws_iam_policy.vm_ssm_access_policy.arn
  role       = element(local.vms_instance_role_names, count.index)
}

resource "aws_iam_policy" "vm_ssm_session_policy" {
  count  = length(local.vms_instance_ids)
  name   = "SSM-start-session-policy-${element(local.vms_instance_ids, count.index)}"
  policy = element(data.template_file.ssm_start_session_policy, count.index).rendered
}

resource "aws_iam_role_policy_attachment" "vm_ssm_session_policy_attach" {
  count      = length(local.vms_instance_role_names)
  policy_arn = element(aws_iam_policy.vm_ssm_session_policy, count.index).arn
  role       = element(local.vms_instance_role_names, count.index)
}

