output "common_globals" {
  description = "Return common globals"
  value       = module.common.globals
}

output "networking_common" {
  description = "Return networking globals"
  value       = module.networking.common
}

output "networking_public_route_table_ids" {
  description = "Return networking public_route_table_ids"
  value       = module.networking.public_route_table_ids
}

output "networking_vpc_id" {
  description = "Return networking vpc id"
  value       = module.networking.vpc_id
}

output "networking_public_subnets" {
  description = "Return public subnets"
  value       = module.networking.public_subnets
}

output "networking_public_subnet_ids" {
  description = "Return public subnets id"
  value       = module.networking.public_subnet_ids
}

output "networking_application_subnets" {
  description = "Return application subnets"
  value       = module.networking.application_subnets
}

output "networking_application_subnet_ids" {
  description = "Return application subnets id"
  value       = module.networking.application_subnet_ids
}

output "networking_application_route_table_ids" {
  description = "Return networking public_route_table_ids"
  value       = module.networking.application_route_table_ids
}

output "networking_data_route_table_ids" {
  description = "Return networking data_route_table_ids"
  value       = module.networking.data_route_table_ids
}

output "networking_application_availability_zones" {
  description = "Return networking application AZ"
  value       = module.networking.application_availability_zones
}

output "dns_public_name_servers" {
  description = "Return public name servers"
  value       = module.dns_zone.public_name_servers
}

output "dns_zone_id" {
  description = "Return public zone id"
  value       = module.dns_zone.zone_id
}

output "dns_zone_name" {
  description = "Return public zone name"
  value       = module.dns_zone.zone_name
}

output "dns_zone_wildcard_certifcate_arn" {
  description = "Return dns_zone wildcard certificate"
  value       = module.dns_zone.wildcard_certificate_arn
}
