terraform {
  required_providers {
    aws      = ">= 3.0.0, < 3.37.0"
    template = ">= 2.1.2"
    null     = ">= 2.1.2"
  }
}