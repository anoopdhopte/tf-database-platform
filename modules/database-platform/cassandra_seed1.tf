module "cassandra_seed1_vm" {
  source                = "git::https://bitbucket.org/anoopdhopte/ec2.git//modules/instance?ref=1.0.6"
  INSTANCE_NAME         = "cass-seed1"
  globals               = module.common.globals
  networking            = module.networking.common
  KMS_KEY_ID            = module.encryption.kms_key_id
  SUBNET_ID             = module.networking.data_subnet_ids[0]
  AWS_AMI_NAME          = var.CASSANDRA_SEED_AMI_NAME
  INSTANCE_TYPE         = var.CASSANDRA_SEED_INSTANCE_TYPE
  VOLUME_SIZE           = var.CASSANDRA_SEED_VOLUME_SIZE
  VOLUME_TYPE           = var.CASSANDRA_SEED_VOLUME_TYPE
  CUSTOM_USER_DATA      = file("${path.module}/files/cloud-init-cassandra")
  SECURITY_GROUP_IDS    = [aws_security_group.cassandra_seed_sg.id]
  CUSTOM_INGRESS_ACCESS = var.CASSANDRA_SEED_CUSTOM_INGRESS_ACCESS
  CUSTOM_TAGS           = local.autoshutdown_tag
}

resource "aws_ebs_volume" "cassandra_seed1_data_disk" {
  availability_zone = module.networking.data_availability_zones[0]
  size              = var.CASSANDRA_SEED_DATA_SIZE
  encrypted         = true
  kms_key_id        = module.encryption.kms_key_id
  type              = var.CASSANDRA_SEED_DATA_TYPE
  throughput        = var.CASSANDRA_SEED_DATA_THROUGHPUT
  iops              = var.CASSANDRA_SEED_DATA_IOPS
  tags              = { "Name" = "${module.common.globals["Project"]}-${module.common.globals["Environment"]}-cass-seed1-data-disk1" }
}

resource "aws_volume_attachment" "cassandra_seed1_data_disk_attachment" {
  device_name = "/dev/xvdb"
  volume_id   = aws_ebs_volume.cassandra_seed1_data_disk.id
  instance_id = module.cassandra_seed1_vm.instance_id
}

module "cassandra_seed1_dns" {
  source       = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/generic_record?ref=1.0.3"
  ZONE_ID      = module.networking.main_zone_id
  RECORDS      = [module.cassandra_seed1_vm.private_ip]
  DOMAIN_NAMES = ["cass-seed1.${module.networking.main_zone_name}"]
}

module "cassandra_seed1_public_dns" {
  source       = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/generic_record?ref=1.0.3"
  ZONE_ID      = module.dns_zone.zone_id
  RECORDS      = [module.cassandra_seed1_vm.private_ip]
  DOMAIN_NAMES = ["cass-seed1.${module.dns_zone.zone_name}"]
}
