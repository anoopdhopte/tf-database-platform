module "nessus_scanner" {
  source                      = "git::https://bitbucket.org/anoopdhopte/nessus.git//modules?ref=1.0.4"
  count                       = var.ENABLE_NESSUS == true ? 1 : 0
  globals                     = module.common.globals
  networking                  = module.networking.common
  NESSUS_SUBNET               = module.networking.public_subnet_ids[0]
  NESSUS_KEY                  = var.NESSUS_KEY
  NESSUS_INSTANCE_TYPE        = var.NESSUS_INSTANCE_TYPE
  SG_EGRESS_RULES             = var.NESSUS_SG_EGRESS_RULES
  NESSUS_LAMBDAS_TRACING_MODE = var.NESSUS_LAMBDAS_TRACING_MODE
}

resource "aws_security_group_rule" "instances_access_nessus_egress" {
  count                    = var.ENABLE_NESSUS == true ? 1 : 0
  description              = "Allow incoming network flow to nessus from other instances"
  type                     = "egress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "all"
  source_security_group_id = module.nessus_scanner[0].nessus_target_security_group_id
  security_group_id        = module.networking.default_security_group_id
}

resource "aws_security_group_rule" "instances_access_nessus_ingress" {
  count                    = var.ENABLE_NESSUS == true ? 1 : 0
  description              = "Allow outgoing network flow from nessus to other instances"
  type                     = "ingress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "all"
  source_security_group_id = module.nessus_scanner[0].nessus_target_security_group_id
  security_group_id        = module.networking.default_security_group_id
}

resource "aws_security_group_rule" "nessus_access_instances_egress" {
  count                    = var.ENABLE_NESSUS == true ? 1 : 0
  description              = "Allow outgoing network flow from nessus to other instances"
  type                     = "egress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "all"
  source_security_group_id = module.networking.default_security_group_id
  security_group_id        = module.nessus_scanner[0].nessus_target_security_group_id
}

resource "aws_security_group_rule" "nessus_access_instances_ingress" {
  count                    = var.ENABLE_NESSUS == true ? 1 : 0
  description              = "Allow incoming network flow to nessus from other instances"
  type                     = "ingress"
  from_port                = 0
  to_port                  = 65535
  protocol                 = "all"
  source_security_group_id = module.networking.default_security_group_id
  security_group_id        = module.nessus_scanner[0].nessus_target_security_group_id
}
