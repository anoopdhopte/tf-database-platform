provider "aws" {
  region = var.AWS_REGION
  assume_role {
    role_arn     = var.AWS_ASSUME_ROLE
    session_name = "db-platform"
  }
}
