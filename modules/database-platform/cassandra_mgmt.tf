module "cassandra_mgmt_vm" {
  source                = "git::https://bitbucket.org/anoopdhopte/ec2.git//modules/instance?ref=1.0.6"
  INSTANCE_NAME         = "cass-mgmt"
  globals               = module.common.globals
  networking            = module.networking.common
  KMS_KEY_ID            = module.encryption.kms_key_id
  SUBNET_ID             = module.networking.data_subnet_ids[0]
  AWS_AMI_NAME          = var.CASSANDRA_MGMT_AMI_NAME
  INSTANCE_TYPE         = "t3.medium"
  CUSTOM_USER_DATA      = file("${path.module}/files/cloud-init-mgmt")
  SECURITY_GROUP_IDS    = [aws_security_group.cassandra_mgmt_sg.id]
  CUSTOM_INGRESS_ACCESS = var.CASSANDRA_MGMT_CUSTOM_INGRESS_ACCESS
  CUSTOM_TAGS           = local.autoshutdown_tag
}

module "cassandra_mgmt_dns" {
  source       = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/generic_record?ref=1.0.3"
  ZONE_ID      = module.networking.main_zone_id
  RECORDS      = [module.cassandra_mgmt_vm.private_ip]
  DOMAIN_NAMES = ["cass-mgmt.${module.networking.main_zone_name}"]
}

