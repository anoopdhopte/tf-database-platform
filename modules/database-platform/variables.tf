locals {
  autoshutdown_tag = var.AUTOSHUTDOWN == true ? {} : { "cpt:tech:autoshutdown" = "false" }
}

variable "AWS_REGION" {
  description = "Provide aws region"
  type        = string
}

variable "AWS_ASSUME_ROLE" {
  description = "Role to which provider should be assumed"
  default     = ""
  type        = string
}

variable "PROJECT" {
  description = "Provide name of the project"
  type        = string
}

variable "ENVIRONMENT" {
  description = "Provide type of the environment, ex dev|test|stg|prod (max 4 characters)"
  type        = string
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
  type        = string
}

variable "VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  type        = string
}

variable "PUBLIC_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of public cidr blocks per availability zones, ex azX = '10.1.1.0/24'"
  type        = list(string)
}

variable "APPLICATION_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of application cidr blocks per availability zones, ex azX = '10.1.10.0/24'"
  type        = list(string)
}

variable "DATA_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of data cidr blocks per availability zones, ex azX = '10.1.20.0/24'"
  type        = list(string)
}

variable "AUTOSHUTDOWN" {
  description = "Enable autoshutdown of instances"
  default     = false
  type        = bool
}

variable "AURORA_DATABASE_ADMIN_PASSWORD" {
  type        = string
  description = "Database admin password"
}

variable "ALLOWED_CIDR_BLOCKS" {
  type        = list(string)
  description = "List of cidrs allowed to access databases"
  default     = []
}

variable "ALLOWED_CIDR_BLOCKS_WR" {
  type        = list(string)
  description = "List of cidrs allowed to access WaitingRoom databases"
  default     = []
}

variable "ALLOWED_CIDR_BLOCKS_OPENDJ" {
  type        = list(string)
  description = "List of cidrs allowed to access OpenDJ"
  default     = []
}

variable "PUBLIC_KEY_PATH" {
  description = "Provide path to default ssh public key for ec2 and bastion instances"
  default     = ""
  type        = string
}

variable "BASTION_ACCESS_CIDRS" {
  description = "List of CIDRs that will be allowed to access bastion"
  default     = ["194.145.235.0/24:22", "185.130.180.0/22:22"] # Lodz office
  type        = list(string)
}

variable "BASTION_AMI_NAME" {
  description = "AMI name for Cassandra seed1 node"
  type        = string
  default     = "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-*"
}

variable "ADDITIONAL_SSH_KEYS" {
  description = "List of extra ssh keys that should be added to bastion and cassandra VM nodes for ubuntu default user"
  type        = list(string)
  default     = []
}

variable "DNS_ZONE_NAME" {
  description = "Provide dns zone name"
  type        = string
}

### Aurora RDS
variable "AURORA_DB_NAME" {
  type        = string
  description = "Database name"
}

variable "AURORA_ADMIN_USER" {
  type        = string
  description = "Database admin user"
}

variable "AURORA_AUTOSCALING_ENABLED" {
  type        = bool
  default     = true
  description = "Whether to enable cluster autoscaling"
}

variable "AURORA_AUTOSCALING_MIN_CAPACITY" {
  type        = number
  default     = 1
  description = "Minimum number of instances to be maintained by the autoscaler"
}

variable "AURORA_AUTOSCALING_MAX_CAPACITY" {
  type        = number
  default     = 5
  description = "Maximum number of instances to be maintained by the autoscaler"
}

variable "AURORA_ENGINE_MODE" {
  type        = string
  default     = "provisioned"
  description = "The database engine mode. Valid values: `parallelquery`, `provisioned`, `serverless`, check regional availability for serverless"
}

variable "AURORA_ENGINE" {
  type        = string
  default     = "aurora-mysql"
  description = "The name of the database engine to be used for this DB cluster. Valid values: `aurora`, `aurora-mysql`, `aurora-postgresql`, must be aurora for serverless mode"
}

variable "AURORA_ENGINE_VERSION" {
  type        = string
  default     = ""
  description = "The version number of the database engine to use"
}

variable "AURORA_INSTANCE_TYPE" {
  type        = string
  default     = "db.r5.large"
  description = "Instance type to use"
}

variable "AURORA_ENGINE_FAMILY" {
  type        = string
  default     = "aurora-mysql5.7"
  description = "The engine family to be used in parameter groups (for example aurora-mysql5.7). For full list check AWS documentation for DBParameterGroupFamily"
}

### Aurora PG
variable "AURORA_PG_DB_NAME" {
  type        = string
  description = "Database name"
}

variable "AURORA_PG_ADMIN_USER" {
  type        = string
  description = "Database admin user"
}

variable "AURORA_PG_DATABASE_ADMIN_PASSWORD" {
  type        = string
  description = "Database admin password"
}

variable "AURORA_PG_AUTOSCALING_ENABLED" {
  type        = bool
  default     = true
  description = "Whether to enable cluster autoscaling"
}

variable "AURORA_PG_AUTOSCALING_MIN_CAPACITY" {
  type        = number
  default     = 1
  description = "Minimum number of instances to be maintained by the autoscaler"
}

variable "AURORA_PG_AUTOSCALING_MAX_CAPACITY" {
  type        = number
  default     = 5
  description = "Maximum number of instances to be maintained by the autoscaler"
}

variable "AURORA_PG_ENGINE" {
  type        = string
  default     = "aurora-postgresql"
  description = "The name of the database engine to be used for this DB cluster. Valid values: `aurora`, `aurora-mysql`, `aurora-postgresql`, must be aurora for serverless mode"
}

variable "AURORA_PG_ENGINE_VERSION" {
  type        = string
  default     = ""
  description = "The version number of the database engine to use"
}

variable "AURORA_PG_ENGINE_FAMILY" {
  type        = string
  default     = "aurora-postgresql12"
  description = "The engine family to be used in parameter groups (for example aurora-postgresql12). For full list check AWS documentation for DBParameterGroupFamily"
}

variable "AURORA_PG_ENGINE_MODE" {
  type        = string
  default     = "provisioned"
  description = "The database engine mode. Valid values: `parallelquery`, `provisioned`, `serverless`, check regional availability for serverless"
}

variable "AURORA_PG_INSTANCE_TYPE" {
  type        = string
  default     = "db.r5.large"
  description = "Instance type to use"
}

variable "AURORA_PG_ENFORCE_CLIENT_SSL" {
  type        = bool
  default     = false
  description = "Whether to enforce SSL for client connections"
}

variable "AURORA_ENHANCED_MONITORING_INTERVAL" {
  type        = number
  description = "Interval in seconds that metrics are collected, 0 to disable (values can only be 0, 1, 5, 10, 15, 30, 60)"
  default     = 60
}

### Cassandra
# tflint-ignore: terraform_documented_variables
variable "CASSANDRA_MGMT_CUSTOM_INGRESS_ACCESS" {
  type    = list(string)
  default = []
}

# tflint-ignore: terraform_documented_variables
variable "CASSANDRA_SEED_CUSTOM_INGRESS_ACCESS" {
  type    = list(string)
  default = []
}

variable "CASSANDRA_BACKUP_RETENTION" {
  description = "Number of days files should be kept on backup bucket (lifecycle policy). If empty it will be disabled"
  default     = ""
  type        = string
}

variable "CASSANDRA_SEED_AMI_NAME" {
  description = "AMI name for Cassandra seed node"
  type        = string
  default     = "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-*"
}

variable "CASSANDRA_MGMT_AMI_NAME" {
  description = "AMI name for Cassandra mgmt node"
  type        = string
  default     = "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-*"
}

variable "CASSANDRA_SEED_AMI_OWNER" {
  description = "AMI owner for Cassandra seed node"
  type        = string
  default     = "099720109477"
}

variable "CASSANDRA_SEED_INSTANCE_TYPE" {
  description = "Instance type for seed node"
  type        = string
  default     = "r5.large"
}

variable "CASSANDRA_SEED_VOLUME_SIZE" {
  description = "Size in GB for main root EBS"
  type        = number
  default     = 20
}

variable "CASSANDRA_SEED_VOLUME_TYPE" {
  description = "Disk type for main root EBS"
  type        = string
  default     = "gp2"
}

variable "CASSANDRA_SEED_DATA_IOPS" {
  description = "Data disk iops for cassandra seed nodes (for gp3, in iops)"
  type        = number
  default     = 3000
}

variable "CASSANDRA_SEED_DATA_THROUGHPUT" {
  description = "Data disk throughput for cassandra seed nodes (in MiB/s)"
  type        = number
  default     = 125
}

variable "CASSANDRA_SEED_DATA_SIZE" {
  description = "Data disk size for cassandra seed nodes (in GB)"
  type        = number
  default     = 50
}

variable "CASSANDRA_SEED_DATA_TYPE" {
  description = "Disk type for data EBS"
  type        = string
  default     = "gp3"
}

variable "CASSANDRA_REAPER_UI_PORT" {
  description = "Cassandra ReaperUI port"
  type        = string
  default     = "8080"
}

# Redis
variable "REDIS_GROUP_ID" {
  type        = string
  default     = "redis-wr"
  description = "Redis group id"
}

variable "REDIS_AUTH_TOKEN" {
  description = "Auth token for redis cluster"
  type        = string
  default     = null
}

variable "REDIS_NODE_GROUPS" {
  description = "Number of node groups in redis"
  type        = number
  default     = 3
}

variable "REDIS_NODE_TYPE" {
  description = "Type of Redis node to be created"
  type        = string
  default     = "cache.t3.medium"
}

variable "REDIS_ENGINE_VERSION" {
  description = "Version of redis engine to use"
  type        = string
  default     = "5.0.6"
}

variable "REDIS_CUSTOM_PARAMETER_GROUP" {
  description = "Parameter group name"
  type        = string
  default     = ""
}

variable "REDIS_APPLY_IMMEDIATELY" {
  default     = false
  type        = bool
  description = "Specifies whether any modifications to Redis are applied immediately, or during the next maintenance window. Default is false"
}

##NESSUS##
variable "NESSUS_KEY" {
  description = "Key used by nessus to authenticate"
  type        = string
  default     = "35e4c0a85554facf6e7c0a74cc43e54141dfe5da94d51273b26460df3a54547c"
}

variable "NESSUS_INSTANCE_TYPE" {
  description = "Type of ec2 instance used to setup nessus"
  type        = string
  default     = "t3.2xlarge"
}

variable "NESSUS_SG_EGRESS_RULES" {
  description = "List of addresses where nessus should have access"
  type        = list(string)
  default     = ["54.93.254.128/26:443", "18.194.95.64/26:443", "3.124.123.128/25:443", "3.67.7.128/25:443", "35.177.219.0/26:443", "3.9.159.128/25:443", "18.168.180.128/25:443", "18.168.224.128/25:443", "35.177.219.11/32:443", "35.177.219.10/32:443", "162.159.130.83/32:443", "162.159.129.83/32:443"]
}

variable "ENABLE_NESSUS" {
  description = "Boolean value, if set to true starts Nessus on environment"
  type        = bool
  default     = false
}

variable "FLOW_LOGS_ENABLED" {
  default     = false
  description = "Define whether vpc flow logs should be enabled"
  type        = bool
}

variable "FLOW_LOGS_BUCKET_ARN" {
  default     = ""
  description = "Provide vpc flow log s3 bucket arn"
  type        = string
}

variable "FLOW_LOGS_TRAFFIC_TYPE" {
  default     = "REJECT"
  description = "Provide type of vpc traffic to capture (ACCEPT, REJECT, ALL)"
  type        = string
}

variable "MFA_ENABLED" {
  description = "Enable multi-factor authentication"
  default     = false
  type        = bool
}

variable "NESSUS_LAMBDAS_TRACING_MODE" {
  default     = "Active"
  description = "Choose tracing mode for nessus-start and nessus-stop lambdas. Valid Values: Active | PassThrough"
  type        = string
}

### OpenDJ
variable "OPENDJ_CUSTOM_INGRESS_ACCESS" {
  type    = list(string)
  default = []
}

variable "OPENDJ_AMI_NAME" {
  description = "AMI name for OpenDJ node"
  type        = string
  default     = "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-*"
}

variable "OPENDJ_AMI_OWNER" {
  description = "AMI owner for OpenDJ node"
  type        = string
  default     = "099720109477"
}

variable "OPENDJ_INSTANCE_TYPE" {
  description = "Instance type for OpenDJ node"
  type        = string
  default     = "r5.large"
}

variable "OPENDJ_VOLUME_SIZE" {
  description = "Size in GB for main root EBS"
  type        = number
  default     = 20
}

variable "OPENDJ_VOLUME_TYPE" {
  description = "Disk type for main root EBS"
  type        = string
  default     = "gp2"
}

variable "OPENDJ_DATA_IOPS" {
  description = "Data disk iops for OpenDJ nodes (for gp3, in iops)"
  type        = number
  default     = 3000
}

variable "OPENDJ_DATA_THROUGHPUT" {
  description = "Data disk throughput for OpenDJ nodes (in MiB/s)"
  type        = number
  default     = 125
}

variable "OPENDJ_DATA_SIZE" {
  description = "Data disk size for OpenDJ nodes (in GB)"
  type        = number
  default     = 5
}

variable "OPENDJ_DATA_TYPE" {
  description = "Disk type for data EBS"
  type        = string
  default     = "gp3"
}

variable "OPENDJ_BACKUP_IOPS" {
  description = "Backup disk iops for OpenDJ nodes (for gp3, in iops)"
  type        = number
  default     = 3000
}

variable "OPENDJ_BACKUP_THROUGHPUT" {
  description = "Backup disk throughput for OpenDJ nodes (in MiB/s)"
  type        = number
  default     = 125
}

variable "OPENDJ_BACKUP_SIZE" {
  description = "Backup disk size for OpenDJ nodes (in GB)"
  type        = number
  default     = 5
}

variable "OPENDJ_BACKUP_TYPE" {
  description = "Disk type for backup EBS"
  type        = string
  default     = "gp3"
}

