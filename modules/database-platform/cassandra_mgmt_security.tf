resource "aws_security_group" "cassandra_mgmt_sg" {
  name_prefix = "${module.common.globals["Project"]}-${module.common.globals["Environment"]}-cassandra-mgmt"
  vpc_id      = module.networking.common["VpcId"]
  description = "${module.common.globals["Project"]}-${module.common.globals["Environment"]}-cassandra-mgmt-sg"
}

### INCOMING

resource "aws_security_group_rule" "cassandra_mgmt_sg_ingress_9042_tcp" {
  description       = "Allow cluster intercommunication on port 9042 TCP"
  type              = "ingress"
  from_port         = "9042"
  to_port           = "9042"
  protocol          = "tcp"
  cidr_blocks       = var.ALLOWED_CIDR_BLOCKS
  security_group_id = aws_security_group.cassandra_mgmt_sg.id
}

resource "aws_security_group_rule" "cassandra_mgmt_sg_ingress_5432_tcp" {
  description              = "Allow cluster intercommunication on port 5432 TCP"
  type                     = "ingress"
  from_port                = "5432"
  to_port                  = "5432"
  protocol                 = "tcp"
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
  source_security_group_id = module.db-pg.aurora_security_group_id
}

### OUTGOING

resource "aws_security_group_rule" "cassandra_mgmt_sg_egress_8080_tcp_seed" {
  description              = "Allow Reaper UI access from mgmt on 8080 TCP"
  type                     = "egress"
  from_port                = "8080"
  to_port                  = "8080"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_seed_sg.id
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
}

resource "aws_security_group_rule" "cassandra_mgmt_sg_egress_22_tcp_seed" {
  description              = "Allow SSH access from mgmt on 22 TCP"
  type                     = "egress"
  from_port                = "22"
  to_port                  = "22"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_seed_sg.id
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
}

resource "aws_security_group_rule" "cassandra_mgmt_sg_egress_3306_tcp" {
  description              = "Allow access from mgmt on 3306 TCP"
  type                     = "egress"
  from_port                = "3306"
  to_port                  = "3306"
  protocol                 = "tcp"
  source_security_group_id = module.db.aurora_security_group_id
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
}

resource "aws_security_group_rule" "cassandra_mgmt_sg_egress_5432_tcp" {
  description              = "Allow access from mgmt on 5432 TCP"
  type                     = "egress"
  from_port                = "5432"
  to_port                  = "5432"
  protocol                 = "tcp"
  source_security_group_id = module.db-pg.aurora_security_group_id
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
}

resource "aws_security_group_rule" "cassandra_mgmt_sg_egress_9042_tcp" {
  description              = "Allow access from mgmt on 9042 TCP"
  type                     = "egress"
  from_port                = "9042"
  to_port                  = "9042"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_seed_sg.id
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
}

resource "aws_security_group_rule" "cassandra_seed_sg_egress_7070_tcp_mgmt" {
  description              = "Allow cluster TCP access 7070 from mgmt node"
  type                     = "egress"
  from_port                = "7070"
  to_port                  = "7070"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_seed_sg.id
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
}

resource "aws_security_group_rule" "cassandra_seed_sg_egress_9100_tcp_mgmt" {
  description              = "Allow cluster TCP access 9100 from mgmt node"
  type                     = "egress"
  from_port                = "9100"
  to_port                  = "9100"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_seed_sg.id
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
}

resource "aws_security_group_rule" "cassandra_mgmt_sg_egress_8080_tcp_wr_seed" {
  description              = "Allow Reaper UI access from mgmt on 8080 TCP"
  type                     = "egress"
  from_port                = "8080"
  to_port                  = "8080"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_wr_seed_sg.id
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
}

resource "aws_security_group_rule" "cassandra_mgmt_sg_egress_22_tcp_wr_seed" {
  description              = "Allow SSH access from mgmt on 22 TCP"
  type                     = "egress"
  from_port                = "22"
  to_port                  = "22"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_wr_seed_sg.id
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
}

resource "aws_security_group_rule" "cassandra_mgmt_egress_9042_tcp_wr_seed" {
  description              = "Allow access from mgmt on 9042 TCP"
  type                     = "egress"
  from_port                = "9042"
  to_port                  = "9042"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_wr_seed_sg.id
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
}

resource "aws_security_group_rule" "cassandra_wr_seed_sg_egress_7070_tcp_mgmt" {
  description              = "Allow cluster TCP access 7070 from mgmt node"
  type                     = "egress"
  from_port                = "7070"
  to_port                  = "7070"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_wr_seed_sg.id
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
}

resource "aws_security_group_rule" "cassandra_wr_seed_sg_egress_9100_tcp_mgmt" {
  description              = "Allow cluster TCP access 9100 from mgmt node"
  type                     = "egress"
  from_port                = "9100"
  to_port                  = "9100"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.cassandra_wr_seed_sg.id
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
}

resource "aws_security_group_rule" "opendj_sg_egress_22_tcp_mgmt" {
  description              = "Allow SSH access from mgmt on 22 TCP"
  type                     = "egress"
  from_port                = "22"
  to_port                  = "22"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.opendj_sg.id
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
}

resource "aws_security_group_rule" "opendj_sg_egress_1389_tcp_mgmt" {
  description              = "Allow cluster TCP access 1389 from mgmt node"
  type                     = "egress"
  from_port                = "1389"
  to_port                  = "1389"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.opendj_sg.id
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
}

resource "aws_security_group_rule" "opendj_sg_egress_4444_tcp_mgmt" {
  description              = "Allow cluster TCP access 4444 from mgmt node"
  type                     = "egress"
  from_port                = "4444"
  to_port                  = "4444"
  protocol                 = "tcp"
  source_security_group_id = aws_security_group.opendj_sg.id
  security_group_id        = aws_security_group.cassandra_mgmt_sg.id
}

