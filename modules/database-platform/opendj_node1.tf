module "opendj_node1_vm" {
  source                = "git::https://bitbucket.org/anoopdhopte/ec2.git//modules/instance?ref=1.0.6"
  INSTANCE_NAME         = "opendj_node1"
  globals               = module.common.globals
  networking            = module.networking.common
  KMS_KEY_ID            = module.encryption.kms_key_id
  SUBNET_ID             = module.networking.data_subnet_ids[0]
  AWS_AMI_NAME          = var.OPENDJ_AMI_NAME
  INSTANCE_TYPE         = var.OPENDJ_INSTANCE_TYPE
  VOLUME_SIZE           = var.OPENDJ_VOLUME_SIZE
  VOLUME_TYPE           = var.OPENDJ_VOLUME_TYPE
  CUSTOM_USER_DATA      = file("${path.module}/files/cloud-init-opendj")
  SECURITY_GROUP_IDS    = [aws_security_group.opendj_sg.id]
  CUSTOM_INGRESS_ACCESS = var.OPENDJ_CUSTOM_INGRESS_ACCESS
  CUSTOM_TAGS           = local.autoshutdown_tag
}

resource "aws_ebs_volume" "opendj_node1_data_disk" {
  availability_zone = module.networking.data_availability_zones[0]
  size              = var.OPENDJ_DATA_SIZE
  encrypted         = true
  kms_key_id        = module.encryption.kms_key_id
  type              = var.OPENDJ_DATA_TYPE
  throughput        = var.OPENDJ_DATA_THROUGHPUT
  iops              = var.OPENDJ_DATA_IOPS
  tags              = { "Name" = "${module.common.globals["Project"]}-${module.common.globals["Environment"]}-opendj_node1-data-disk1" }
}

resource "aws_ebs_volume" "opendj_node1_backup_disk" {
  availability_zone = module.networking.data_availability_zones[0]
  size              = var.OPENDJ_BACKUP_SIZE
  encrypted         = true
  kms_key_id        = module.encryption.kms_key_id
  type              = var.OPENDJ_BACKUP_TYPE
  throughput        = var.OPENDJ_BACKUP_THROUGHPUT
  iops              = var.OPENDJ_BACKUP_IOPS
  tags              = { "Name" = "${module.common.globals["Project"]}-${module.common.globals["Environment"]}-opendj_node1-backup-disk1" }
}

resource "aws_volume_attachment" "opendj_node1_data_disk_attachment" {
  device_name = "/dev/xvdb"
  volume_id   = aws_ebs_volume.opendj_node1_data_disk.id
  instance_id = module.opendj_node1_vm.instance_id
}

resource "aws_volume_attachment" "opendj_node1_backup_disk_attachment" {
  device_name = "/dev/xvdc"
  volume_id   = aws_ebs_volume.opendj_node1_backup_disk.id
  instance_id = module.opendj_node1_vm.instance_id
}

module "opendj_node1_dns" {
  source       = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/generic_record?ref=1.0.3"
  ZONE_ID      = module.networking.main_zone_id
  RECORDS      = [module.opendj_node1_vm.private_ip]
  DOMAIN_NAMES = ["opendj-node1.${module.networking.main_zone_name}"]
}

module "opendj_node1_public_dns" {
  source       = "git::https://bitbucket.org/anoopdhopte/dns.git//modules/generic_record?ref=1.0.3"
  ZONE_ID      = module.dns_zone.zone_id
  RECORDS      = [module.opendj_node1_vm.private_ip]
  DOMAIN_NAMES = ["opendj-node1.${module.dns_zone.zone_name}"]
}


