locals {
  environment_name = "${var.PROJECT}-${var.ENVIRONMENT}"
}

variable "AWS_REGION" {
  description = "Provide aws region"
  default     = "eu-central-1"
  type        = string
}

variable "PROJECT" {
  description = "Provide name of the project"
  type        = string
}

variable "ENVIRONMENT" {
  description = "Provide type of the environment, ex dev|test|stg|prod (max 4 characters)"
  default     = "test"
  type        = string
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
  type        = string
}

variable "VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "10.1.0.0/24"
  type        = string
}

variable "PUBLIC_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of public cidr bloks per availability zones, ex azX = '10.1.1.0/24'"
  type        = list(string)
}

variable "APPLICATION_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of application cidr bloks per availability zones, ex azX = '10.1.10.0/24'"
  type        = list(string)
}

variable "DATA_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of data cidr bloks per availability zones, ex azX = '10.1.20.0/24'"
  type        = list(string)
}

variable "ALLOWED_CIDR_BLOCKS" {
  type        = list(string)
  description = "List of cidrs allowed to access database"
  default     = []
}

variable "ALLOWED_CIDR_BLOCKS_WR" {
  type        = list(string)
  description = "List of WR cidrs allowed to access database"
  default     = []
}

variable "AURORA_DATABASE_ADMIN_PASSWORD" {
  type        = string
  default     = ""
  description = "Database admin password"
}

variable "AURORA_DB_NAME" {
  type        = string
  default     = ""
  description = "Database name"
}

variable "AURORA_ADMIN_USER" {
  type        = string
  default     = ""
  description = "Database admin user"
}

variable "AURORA_ENGINE_VERSION" {
  type        = string
  default     = ""
  description = "The version number of the database engine to use"
}

variable "AURORA_INSTANCE_TYPE" {
  type        = string
  default     = "db.r5.large"
  description = "Instance type to use"
}

variable "AURORA_WR_ENGINE_VERSION" {
  type        = string
  default     = ""
  description = "The version number of the database engine to use"
}

variable "AURORA_WR_INSTANCE_TYPE" {
  type        = string
  default     = "db.r5.large"
  description = "Instance type to use"
}

variable "AURORA_WR_DATABASE_ADMIN_PASSWORD" {
  type        = string
  default     = ""
  description = "Database admin password"
}

variable "AURORA_WR_DB_NAME" {
  type        = string
  default     = ""
  description = "Database name"
}

variable "AURORA_WR_ADMIN_USER" {
  type        = string
  default     = ""
  description = "Database admin user"
}

variable "AURORA_PG_ENGINE_VERSION" {
  type        = string
  default     = ""
  description = "The version number of the database engine to use"
}

variable "AURORA_PG_INSTANCE_TYPE" {
  type        = string
  default     = "db.r5.large"
  description = "Instance type to use"
}

variable "AURORA_PG_DATABASE_ADMIN_PASSWORD" {
  type        = string
  default     = ""
  description = "Database admin password"
}

variable "AURORA_PG_DB_NAME" {
  type        = string
  default     = ""
  description = "Database name"
}

variable "AURORA_PG_ADMIN_USER" {
  type        = string
  default     = ""
  description = "Database admin user"
}

variable "DNS_ZONE_NAME" {
  description = "Provide dns zone name"
  default     = ""
  type        = string
}

variable "BASTION_ACCESS_CIDRS" {
  description = "List of CIDRs that will be allowed to access bastion"
  type        = list(string)
  default     = []
}

variable "ADDITIONAL_SSH_KEYS" {
  description = "List of extra ssh keys that should be added to bastion and cassandra VM nodes for ubuntu default user"
  type        = list(string)
  default     = []
}

variable "CASSANDRA_SEED_INSTANCE_TYPE" {
  description = "Instance type for seed node"
  type        = string
  default     = "r5.large"
}

variable "CASSANDRA_SEED_VOLUME_SIZE" {
  description = "Size in GB for main root EBS"
  type        = string
  default     = "20"
}

variable "CASSANDRA_SEED_DATA_SIZE" {
  description = "Size in GB for main root EBS"
  type        = string
  default     = "20"
}

variable "CASSANDRA_SEED_DATA_IOPS" {
  description = "Data disk iops for cassandra seed nodes (for gp3, in iops)"
  type        = number
  default     = 3000
}

variable "CASSANDRA_SEED_DATA_THROUGHPUT" {
  description = "Data disk throughput for cassandra seed nodes (in MiB/s)"
  type        = number
  default     = 125
}

variable "OPENDJ_AMI_NAME" {
  description = "AMI name for OpenDJ node"
  type        = string
  default     = "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20220118"
}

variable "OPENDJ_INSTANCE_TYPE" {
  description = "Instance type for OpenDJ node"
  type        = string
  default     = "r5.large"
}

variable "OPENDJ_VOLUME_SIZE" {
  description = "Size in GB for main root EBS"
  type        = number
  default     = 20
}

variable "OPENDJ_VOLUME_TYPE" {
  description = "Disk type for main root EBS"
  type        = string
  default     = "gp2"
}

variable "OPENDJ_DATA_IOPS" {
  description = "Data disk iops for OpenDJ nodes (for gp3, in iops)"
  type        = number
  default     = 3000
}

variable "OPENDJ_DATA_THROUGHPUT" {
  description = "Data disk throughput for OpenDJ nodes (in MiB/s)"
  type        = number
  default     = 125
}

variable "OPENDJ_DATA_SIZE" {
  description = "Data disk size for cassandra seed nodes (in GB)"
  type        = number
  default     = 5
}

variable "OPENDJ_BACKUP_IOPS" {
  description = "Backup disk iops for OpenDJ nodes (for gp3, in iops)"
  type        = number
  default     = 3000
}

variable "OPENDJ_BACKUP_THROUGHPUT" {
  description = "Backup disk throughput for OpenDJ nodes (in MiB/s)"
  type        = number
  default     = 125
}

variable "OPENDJ_BACKUP_SIZE" {
  description = "Backup disk size for cassandra seed nodes (in GB)"
  type        = number
  default     = 5
}

variable "REDIS_GROUP_ID" {
  type        = string
  default     = "redis-wr"
  description = "Redis group id"
}

variable "REDIS_AUTH_TOKEN" {
  description = "Auth token for redis cluster"
  type        = string
  default     = null
}

variable "CASSANDRA_SEED_AMI_NAME" {
  description = "AMI name for Cassandra seed1 node"
  type        = string
  default     = "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20210415"
}

variable "BASTION_AMI_NAME" {
  description = "AMI name for Cassandra seed1 node"
  type        = string
  default     = "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20210415"
}

variable "OBS_AWS_ACCOUNT_ID" {
  description = "AWS account ID for OBS cluster"
  default     = ""
  type        = string
}

variable "CASSANDRA_MGMT_AMI_NAME" {
  description = "AMI name for Cassandra management node"
  type        = string
  default     = "ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-20210623"
}

##NESSUS##
variable "NESSUS_KEY" {
  description = "Key used by nessus to authenticate"
  type        = string
  default     = "35e4c0a85554facf6e7c0a74cc43e54141dfe5da94d51273b26460df3a54547c"
}

variable "NESSUS_INSTANCE_TYPE" {
  description = "Type of ec2 instance used to setup nessus"
  type        = string
  default     = "t3.2xlarge"
}

variable "NESSUS_SG_EGRESS_RULES" {
  description = "List of addresses where nessus should have access"
  type        = list(string)
  default     = ["54.93.254.128/26:443", "18.194.95.64/26:443", "3.124.123.128/25:443", "3.67.7.128/25:443", "35.177.219.0/26:443", "3.9.159.128/25:443", "18.168.180.128/25:443", "18.168.224.128/25:443", "35.177.219.11/32:443", "35.177.219.10/32:443", "162.159.130.83/32:443", "162.159.129.83/32:443"]
}

variable "ENABLE_NESSUS" {
  description = "Boolean value, if set to true starts Nessus on environment"
  type        = bool
  default     = false
}

variable "AWS_ASSUME_ROLE" {
  description = "Role to which provider should be assumed"
  default     = ""
  type        = string
}

variable "AURORA_AUTOSCALING_ENABLED" {
  type        = bool
  default     = true
  description = "Whether to enable cluster autoscaling"
}

variable "AURORA_WR_AUTOSCALING_ENABLED" {
  type        = bool
  default     = true
  description = "Whether to enable cluster autoscaling"
}

variable "AURORA_PG_AUTOSCALING_ENABLED" {
  type        = bool
  default     = true
  description = "Whether to enable cluster autoscaling"
}

