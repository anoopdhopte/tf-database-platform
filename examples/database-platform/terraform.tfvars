AWS_REGION             = "eu-central-1"
PROJECT                = "database-pl"
ENVIRONMENT            = "database"
ACCOUNT_NAME           = "teribu"
VPC_CIDR_BLOCK         = "10.0.0.0/16"
DATA_CIDR_BLOCKS       = ["10.0.20.0/24", "10.0.21.0/24", "10.0.22.0/24"]
PUBLIC_CIDR_BLOCKS     = ["10.104.30.0/24", "10.104.31.0/24", "10.104.32.0/24"]
ALLOWED_CIDR_BLOCKS    = ["10.4.0.0/16", "10.6.0.0/16", "10.3.0.0/16"]
ALLOWED_CIDR_BLOCKS_WR = ["10.4.0.0/16", "10.6.0.0/16", "10.3.0.0/16"]

DATABASE_ADMIN_PASSWORD = "test_password"
ADMIN_USER              = "master"
AUTOSCALING_ENABLED     = true
ENGINE_MODE             = "serverless"
ENGINE                  = "aurora-mysql"
ENGINE_VERSION          = "5.7"

AURORA_DATABASE_ADMIN_PASSWORD = "ZGF0YmFzZXBhc3N3b3JkMTIzIQo="
AURORA_DB_NAME                 = "oneplatformdb"
AURORA_ADMIN_USER              = "master"
AURORA_ENGINE_VERSION          = "5.7.mysql_aurora.2.07.2"
AURORA_INSTANCE_TYPE           = "db.r5.large"
AURORA_AUTOSCALING_ENABLED     = false

AURORA_WR_DATABASE_ADMIN_PASSWORD = "POPJ2xYBe76EGmed"
AURORA_WR_DB_NAME                 = "oneplatformwrdb"
AURORA_WR_ADMIN_USER              = "master"
AURORA_WR_ENGINE_VERSION          = "5.7.mysql_aurora.2.07.2"
AURORA_WR_INSTANCE_TYPE           = "db.r5.large"
AURORA_WR_AUTOSCALING_ENABLED     = false

AURORA_PG_DATABASE_ADMIN_PASSWORD = "WUf5rXRg4ePSw59nD5YNtsSi"
AURORA_PG_DB_NAME                 = "oneplatformpgdb"
AURORA_PG_ADMIN_USER              = "master"
AURORA_PG_ENGINE_VERSION          = "12.6"
AURORA_PG_INSTANCE_TYPE           = "db.r5.large"
AURORA_PG_AUTOSCALING_ENABLED     = false

REDIS_AUTH_TOKEN = "kfspCcgOFABdHBktvn5lNR6fPUb1F5xq"

CASSANDRA_SEED_INSTANCE_TYPE   = "r5.large"
CASSANDRA_SEED_VOLUME_SIZE     = "20"
CASSANDRA_SEED_DATA_SIZE       = "50"
CASSANDRA_SEED_DATA_THROUGHPUT = "125"
CASSANDRA_SEED_DATA_IOPS       = "3000"

OPENDJ_INSTANCE_TYPE     = "r5.large"
OPENDJ_VOLUME_SIZE       = "10"
OPENDJ_DATA_SIZE         = "50"
OPENDJ_DATA_THROUGHPUT   = "125"
OPENDJ_DATA_IOPS         = "3000"
OPENDJ_BACKUP_SIZE       = "50"
OPENDJ_BACKUP_THROUGHPUT = "125"
OPENDJ_BACKUP_IOPS       = "3000"

BASTION_ACCESS_CIDRS = [
  "194.145.235.0/24:22", # Lodz office
  "185.130.180.0/22:22", # Lodz office
  "31.183.195.127/32:22" # Stanislaw Wos home
]

ENABLE_NESSUS = false
//MONITORING_CLUSTER_INTERNAL_LOAD_BALANCER = "internal-observability--in-alb-a8rh5q-1004210706.eu-west-3.elb.amazonaws.com"
//MONITORING_CLUSTER_DOMAIN                 = "obs.staging.identity-dev.idemia.io"
DNS_ZONE_NAME = "db.staging.identity-dev.idemia.io"

ADDITIONAL_SSH_KEYS = [
  "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDCmMrAD5Nzd3zTvsxy83chddWrxcaDvGPhK0Okm4sHL839OYJ7em9POIo7oeFYGshbDMJvG1drdTe+d2Ri8UV7JJ//RsxO6dryL3vBBIHGL3RrVGelLiPTT/S7DeW4+3pT34BEC7llQpBp7QQ8T5Gg3Tl7QgzRWlDL18j80sZKXCQl1Bfwlq9XvgGifSjAIOt51OTZtlA/rhjHYmTX7ybnKJHGNa/JhWc62MlGISDQzOSPp/rqkEj1VgjY0KUrW64Is4b03EGtX86R406Uc8H8Y229uWUKctFu8Nt9MVQLGv8w8mQSRtrjR2WlZfYjKtsqS+JzeRTBFdxx6b+uouyVzIsZqa3GHStWwt+fQT4+OlK9rJ7ch7fY5ojeoOxXqNCn1LoCE+cJTkpOTWUkJVEDUtmoON9z4o08hIWpQpl1GxCMXqawEnQcrec7dSc2UckocxrGs24PEvBMFzf4qNjpqV4ZJMgn3N3warEvtyr4nPbmFUwUhbpq+eROW2jHCKdhlZ9s3j2oT69lFTKeakBcImy1jX/XoZPzXAkAp0qHUHVEwpx1+y0OW6r+H3T0xBC229hAigyF7bNT1j5JRJj4RQaXdoorRhLda26b9NIxDvabvPr1bmmB6+uWFwbo5/H0F0riYd274ftoX406ubJmtdbclZ7zPlYAMuZp5bBe2Q==", # DBA-TEAM
  "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC2eBmDAGIVZFggeuiFqVeaSyQqkJggLi4/+zEXGX5oRu1KbYgw88Yzs67PtenlxNLhsgDYGBxGp6m1zcrVI1+0/FJctPGVn6CHTRIurS4e+t4LhGdEHAt+Vg2BlAKZF2iBgPcVjiST6UzGfqoB0IuK10JdOdXitRYNRE+6uX09bDCtXLJXmidYRgXRiY/Y2bFaNlHSkv9gp2NQtetmSMEg/l2aX/XGn5FdewuHKn9rdsaj918MEft70lGFhMywpSRKzR4WRG50lyeQJkWJMnlGMJyXWUVKN98ZxiN4RGT/wQafFIG7nQUiEStcEYBmQwm486ueLCh/kF1qcj5CLLmapNVnVbYan595c3WPglYhoj6BtR6n7MD5vs2+0dneYcP7nWjJ0tItZGBY7Bna+XQd0dERHtTeI4fLvfyJzjcNfAxQSqmpevIKUJGKFxi2CE1+FpJ1OXDinXVMGL9dHVFdEliB3GtUWDDAAs0WorOj8FeEs5W0Wg+PfS1w7Og8aAKY6cnSjeaZwz9rCjC5K7QHJ2bNK++EvS752pHT2cBc5/uCVaximMxvypl/aFrexftVxJpuvxmylIaEjbucxBtYy12vF5RTeSi4KqwiOXG0oeRZosH8NaHyCRrBqSUJOnlFXIxBrKahhAxaPILnGrLe9ZPSC7yQubQ4jWWW2E98Vw==", # DBA-TEAM
  "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCqHlek7VPIXK+W21luENldzmfFlHB1fg+O8Z4meVg4mjZKBypwQvguywrfxL/NMWzvNE2qqNQu8dxzWtAdoV6NTt2GV3tRktA+ZNxuz6pZLvZ77McwwG6yZ2a5SyvUp3sz0TXN/H8bxFclTShayFt8bJBGYmXcT8jGz1SRbBGTBS+cl+x5AoeIAc5eM6ALPk6f7R7RZfkIlIoZyThr+br7E8jKSatkw07MBNFvDVdFjVCIgBUv9ih/x7tCdJeaDjiz+/WLkj1Pi06Hmdtu86Fb71dUa/3KLywHilsppv19uLYDQ6u83SJ5gdiqlXcynyc2pRpipFbld7wsXKHaeABx",                                                                                                                                                                                                                                                                                                                                                         # DBA-TEAM
  "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDCDnDvAu3wrwManHDRAv5Er6cTfFUdFjGBshmmhsgRNQZsnkUIqN6j6V3hVTjSzUicO2ERGYgFqY696ccHWYQqynJrarWfh7qdZCNITqDXEYFnBYf0YjNSlnMI94Qst2TISAk3B2xwsFAqKU3nHjwLsjZhVxq1DwaWdNyXwHhWt/q7h4ncw9T17BK0OuuGIP6231L37IiJ90+bQcQxuaN+MymFL1acriFrxS9CmPVtmLOZPWmssiQZzr4jp5dWdmM+1nFDno125pE7y7IBS8pSzz2IrHtNMQ9D8g2BUhI72r3SxhBvy9FcWpco5+ZF8HSG9WTD+l3bZt48MIcyHYLLa3F5l2V2R4ge7RhSY92C9RlBzZoc6E1LJ8RNE3K1iuBct1EE+k4O44tB3Z7uiZL7XyUPNI0LlAtwsx10VUrkYk3TK3qzpOahX4HO/4d+rmJQce42uRVkWsUHdMXusm22i5XdjDAzXiowHGCTTCiS/f/lz7xiza9gTaFZSgseLuU= gerard.stanczak@idemia.com"
]

OBS_AWS_ACCOUNT_ID = "054147515276"