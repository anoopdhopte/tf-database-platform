# Change Log
-------------
## 1.4.1 2022-03-01
Code adjusted to support multiple instances of Database platform on the same account

# Change Log
-------------
## 1.3.1 2022-02-02
Flows and volumes for OpenDJ

-------------
## 1.3.0 2022-01-26
OpenDJ infrastructure
CassandraWR access from management node

-------------
## 1.2.9 2022-01-13
Includes updates to nessus and elasticache modules to patch up security vulnerabilities.
Stylistic/linter cleanup

## 1.2.8 2021-12-15
correction for backward compatibility break introduced in 1.2.6 
(cassandra seed names must stay the same to avoid breaking existing cassandra on platform updates)

## 1.2.7 2021-12-10
Added connection between ec2 instances and nessus scanner instance

## 1.2.6 2021-12-10
reduce number of character for cassandra seed names
rename S3 bucket for logs to avoid conflict with S3 from other platforms

## 1.2.5 2021-12-01
Changed the monitoring interval of enhanced monitoring for Aurora RDS cluster.

## 1.2.4 2021-11-10
Added IAM role for Aurora RDS to send enhanced metrics to Cloudwatch.

## 1.2.3 2021-11-09 includes a change 1.2.2RC1
Update nessus scanner version
Inline policies replaced by managed policies

## 1.2.1 2021-10-20
Parametrized values for autoscaler for aurora
Parametrized redis/elasticache version 

## 1.2.0 2021-09-22
Added S3 policy to ensure secure bucket access
Add Aurora PostgreSQL
Remove Aurora MySQL for WR
Adjust security rules from management node
Added access to EC2 instances via SessionManager 

## 1.1.4 2021-08-31
Add VPC flows logging
Fix session name
Fix logs var
Fix providers

## 1.1.3 2021-08-27
Allow 9042 egress from bastion to cassandra_seed_sg

## 1.1.2 2021-08-25
Fix examples
Add Nessus
Add assume role to provider
Allow 9042 ingress from bastion

## 1.1.1 2021-08-24
Add custom tags to ec2 instances

## 1.1.0 2021-06-24
Add management node
Refactored cloudinit
Adjust security rules for prometheus monitoring & reaper UI

## 1.0.0RC2 2021-05-06
Added Aurora and Elasticache for Waiting Room
Added IAM for s3 backup bucket
Rebuilt bastion to use standalone ec2

## 1.0.0RC1 2021-04-19
Added Cassandra ec2-based cluster + s3 backup bucket

## 1.0.1 2020-02-26
GID-1129: Release and provisioned mode

## 1.0.0 2020-02-04
Init release 

## 0.1.0 2021-01-19
Initial package

#### Added
- Initial package for database-platform
